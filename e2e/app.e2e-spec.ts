import { ExamfrondendPage } from './app.po';

describe('examfrondend App', () => {
  let page: ExamfrondendPage;

  beforeEach(() => {
    page = new ExamfrondendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
