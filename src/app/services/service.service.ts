import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ServiceService {

  constructor(private http:Http) { }

  getData(){
    return this.http.get('https://jsonplaceholder.typicode.com/users')
    // .map(result => result.json())
    // .catch(error => Observable.throw(error.json().error) || "Server Error")
  }

  form : object[] = [
    {"ID":null, "Name":"", "Email":"", "Address":"", "Phone": null, "Company":""},
  ];

  newId : number;
  newName : string = "";
  newEmail : string = "";
  newAddress : string = "";
  newPhone : number;
  newCompany : string = "";


   addToList(){
    if (this.form.length == 0) {
      this.newId = 1;
    }
    else{
      this.newId = this.form[this.form.length - 1]['ID']+1;
    }

    if (this.newName != "" &&
        this.newAddress != "" &&
        this.newEmail != "") 
        
        {
      this.form.push({"ID":this.newId, "Name":this.newName, "Address":this.newAddress, "Email":this.newEmail, "Phone":this.newPhone, "Company":this.newCompany});
      }      
    }
  
 

}
