import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../services/service.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

   data : object [];

  constructor(private api:ServiceService) { }

  ngOnInit() {
    this.api.getData()
    .subscribe(result => this.data = result.json() );
  }

  remove(index){
    this.data.splice (index, 1);
  }

}
